﻿using UnityEngine;
using System.Collections;

public class Paddle2AI : MonoBehaviour {
	public Transform target;

	public Rigidbody bod;

	public float force;
	// Use this for initialization
	void Start () {
		bod = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 targetPos = new Vector3 (target.position.x,target.position.y,this.transform.position.z);

		this.transform.LookAt (targetPos);
		//transform.Rotate(Vector3(-90,0,0));

		this.bod.AddForce (Vector3.forward*force);
	}


}
