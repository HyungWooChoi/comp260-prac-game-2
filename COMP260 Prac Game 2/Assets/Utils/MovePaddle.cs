﻿using UnityEngine;
using System.Collections;

//MovePosition simply sets a coordinate for the object to be at.
//Velocity sets a speed at which the object simply moves without care of objects front of it.
//AddForce constantly adds more velocity to the object. Like as though it is being pushed.
[RequireComponent (typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

	public KeyCode Up;
	public KeyCode Right;
	public KeyCode Down;
	public KeyCode Left;

	private Rigidbody rigidbody;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	/*void FixedUpdate () {
		Vector3 pos = GetMousePosition();
		rigidbody.MovePosition(pos);
	}*/

	/*public float speed = 1f;

	void FixedUpdate () {
		Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rigidbody.position;
		dir = dir.normalized;
		Vector3 vel = dir.normalized * speed;
		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;

	}*/

	public float force = 10f;

	void FixedUpdate () {
		/*Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rigidbody.position;

		rigidbody.AddForce(dir.normalized * force);*/
		playerInput ();
	}

	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XY plane
		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}

	void playerInput()
	{
		if(Input.GetKey(Up))
		{
			rigidbody.AddForce (Vector3.up*force);

		}
		if(Input.GetKey(Right))
		{
			rigidbody.AddForce (Vector3.right*force);
		}
		if(Input.GetKey(Down))
		{
			rigidbody.AddForce (-Vector3.up*force);
		}
		if(Input.GetKey(Left))
		{
			rigidbody.AddForce (-Vector3.right*force);
		}
	}
}
