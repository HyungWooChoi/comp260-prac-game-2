﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	private AudioSource audio;
	public Transform startingPos;

	void Start () {
		audio = GetComponent<AudioSource> ();
		ResetPosition();
	}
		
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;

	void OnCollisionEnter(Collision collision) {
		// check what we have hit
		if (paddleLayer.Contains(collision.gameObject)) {
			// hit the paddle
			audio.PlayOneShot(paddleCollideClip);
		}
		else {
			// hit something else
			audio.PlayOneShot(wallCollideClip);
		}
	}

	public void ResetPosition() {
		// teleport to the starting position
		GetComponent<Rigidbody>().MovePosition(startingPos.position);
		GetComponent<Rigidbody>().velocity = Vector3.zero;
	}


	// Update is called once per frame
	void Update () {
	
	}

	/*void OnCollisionEnter(Collision col)
	{
		Debug.Log ("Collided");
	}

	void OnCollisionStay(Collision col)
	{
		Debug.Log ("Still Touching");
	}

	void OnCollisionExit(Collision col)
	{
		Debug.Log ("Bad Touch");
	}
	*/

	/*void OnCollisionEnter(Collision collision) {
		Debug.Log("Collision Enter: " + collision.gameObject.name);
	}

	void OnCollisionStay(Collision collision) {
		Debug.Log("Collision Stay: " + collision.gameObject.name);
	}

	void OnCollisionExit(Collision collision) {
		Debug.Log("Collision Exit: " + collision.gameObject.name);
	}
	*/
}
